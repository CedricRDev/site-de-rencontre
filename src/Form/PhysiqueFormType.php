<?php

namespace App\Form;

use App\Entity\Physique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhysiqueFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        // Section Physique

            // Je dois trouver comment mettre un choix en scrolling pour la taille!!!!
           ->add('taille', ChoiceType::class, [
                'choices'=> [
                    'petit' => 'petit',
                    'Moyen' => 'Moyen',
                    'Grand'=> 'Grand'
            ]])
             ->add('silhouette', ChoiceType::class, [
                'choices'=> [
                    'Mince' => 'Mince',
                    'Normal'=> 'Normal',
                    'Des kilos en trop'=> 'Des kilos en trop',
                    'Trapu' => 'Trapu'
             ]])
             ->add('style', ChoiceType::class, [
                'choices'=> [
                    'Fashion' => 'Fashion',
                    'Décontracté '=> 'Décontracté ',
                    'Classique'=> 'Classique',
                    'Sportif'=>'Sportif'
             ]])
             ->add('origine', ChoiceType::class, [
                'choices'=> [
                    'Européenne' => 'Européenne',
                    'Méditéranéenne'=> 'Méditéranéenne',
                    'Asiatique'=> 'Asiatique',
                    'Arabe'=> 'Arabe',
                    'Africaine'=> 'Africaine',
             ]])
             ->add('yeux', ChoiceType::class, [
                'choices'=> [
                    'Bleu' => 'Bleu',
                    'Vert'=> 'Vert',
                    'Marron'=> 'Marron',
                    'Noir'=> 'Noir',
             ]])
             ->add('cheveux', ChoiceType::class, [
                'choices'=> [
                    'Brun' => 'Brun',
                    'Blond'=> 'Blond',
                    'Chatain'=> 'Chatain',
                    'Gris'=> 'Gris',
             ]])
             ->add('longueur_cheveux', ChoiceType::class, [
                'choices'=> [
                    'Court' => 'Court',
                    'Long'=> 'Long',
                    'Rasé'=> 'Rasé',
                    'Aucun'=> 'Aucun',
             ]])
             ->add('submit', SubmitType::class, [
                'label' => 'Modify Physique'
            ])
            
            ->getForm();
             ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> Physique::class
        ]);
    }
}