<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=PersonalityRepository::class)
 */
class Personality 
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $relation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $romantique;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mariage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enfant;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="personality")
     */
    private $users;

    public function __construct()
    {
        $this->features = new ArrayCollection();
        $this->users = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->relation;
    }  
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRelation(): ?string
    {
        return $this->relation;
    }

    public function setRelation(?string $relation): self
    {
        $this->relation = $relation;

        return $this;
    }

    public function getRomantique(): ?string
    {
        return $this->romantique;
    }

    public function setRomantique(?string $romantique): self
    {
        $this->romantique = $romantique;

        return $this;
    }

    public function getMariage(): ?string
    {
        return $this->mariage;
    }

    public function setMariage(?string $mariage): self
    {
        $this->mariage = $mariage;

        return $this;
    }

    public function getEnfant(): ?string
    {
        return $this->enfant;
    }

    public function setEnfant(?string $enfant): self
    {
        $this->enfant = $enfant;

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setPersonality($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getPersonality() === $this) {
                $user->setPersonality(null);
            }
        }

        return $this;
    }
}
