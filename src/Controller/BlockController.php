<?php

namespace App\Controller;

use App\Entity\Block;
use App\Entity\Conversation;
use App\Repository\BlockRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

  

class BlockController extends AbstractController
{
    /**
     * @Route("/block/{id}", name="user_block")
     */
    public Function blockUser( int $id, UserRepository $userRepository)
    {
        $blockUser = new Block;
        $user = $this->getUser();

        $entityManager = $this
                ->getDoctrine()
                ->getManager();

        // on recupere l'Id de l'utilisateur et on l'enregistre
        $blockUser->setRequestUser($user);

        // on recupere l'id dans l'URL et on l 'enregistre en tant qus TargetUser
        $userTarget= $userRepository->findOneBy(["id"=>$id]);   
        $blockUser->setTargetUser($userTarget);

        $entityManager->persist($blockUser);
        $entityManager->flush();
        return $this->redirectToRoute('accueil');
    }
    
    /**
     * @Route("/block/all-view/{id}", name="view_user_block")
     */
    public Function viewblockUser( int $id, BlockRepository $blockRepository)
    {
        $blockRepository = $this
        ->getDoctrine()
        ->getRepository(Block::class);  
        $blockUser = $blockRepository-> findAll();
        
        // on recupere  l'id l'utilisateur connecté
        $userId = $this->getUser()->getId();

         // on recupere l'ID de de celui qui a fait la requete de blocage
         $blockUserId  = $blockUser["0"]->getRequestUser()->getId();

         foreach ($blockUser as $blockUsers)
         {
            $blockUser = $blockRepository-> findBy(["requestUser" => $blockUsers->getRequestUser()->getId()]);
            
            if ($userId === $blockUserId )
            { 
                //  SI l'ID de l'utilisateur connecte est retouve on affiche les users bloqués
                 $blockUser = $blockRepository-> findBy(["requestUser" => $blockUsers->getRequestUser()->getId()]);

             // Si l'id, ne correspond pas, on renvoie une chaine vide 
            }elseif (($userId != $blockUsers->getRequestUser()->getId() )){
                $blockUser ="";
            }
        }
       return $this->render('blackList/blackList.html.twig',
        [
            "blockUser"=> $blockUser]
        );
    }

    /**
     * @Route("/block/delete/{id}", name="delete_user_block")
     */
    public Function deleteBlockUser( int $id)
    {
        $user = $this->getUser();
        //dd($user);
         $entityManager = $this
                    ->getDoctrine()
                    ->getManager();
         $blockUser = $entityManager->getRepository(Block::class)->find($id);

            if (!$blockUser) {
                throw $this->createNotFoundException(
                    'No User found on your Black list'
                );
            }
            $entityManager->remove($blockUser);
            $entityManager->flush();
        
       return $this->redirectToRoute('accueil', ["id"=>$user->getid()]);
    }
}