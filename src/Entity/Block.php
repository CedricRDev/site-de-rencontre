<?php

namespace App\Entity;

use App\Repository\BlockRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BlockRepository::class)
 */
class Block
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="blocks")
     */
    private $requestUser;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="blocks")
     */
    private $targetUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequestUser(): ?User
    {
        return $this->requestUser;
    }

    public function setRequestUser(?User $requestUser): self
    {
        $this->requestUser = $requestUser;

        return $this;
    }

    public function getTargetUser(): ?User
    {
        return $this->targetUser;
    }

    public function setTargetUser(?User $targetUser): self
    {
        $this->targetUser = $targetUser;

        return $this;
    }
}
