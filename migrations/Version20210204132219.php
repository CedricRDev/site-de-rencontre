<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210204132219 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD personality_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649CF3DE080 FOREIGN KEY (personality_id) REFERENCES personality (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649CF3DE080 ON user (personality_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649CF3DE080');
        $this->addSql('DROP INDEX IDX_8D93D649CF3DE080 ON user');
        $this->addSql('ALTER TABLE user DROP personality_id');
    }
}
