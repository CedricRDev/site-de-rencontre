<?php

namespace App\Entity;

use App\Repository\LifeStyleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LifeStyleRepository::class)
 */
class LifeStyle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fume;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enfant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profession;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $religion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pratique;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mange;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $animaux;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="lifeStyle")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }
    public function __toString()
    {
        return $this->statut;
    }   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getFume(): ?string
    {
        return $this->fume;
    }

    public function setFume(?string $fume): self
    {
        $this->fume = $fume;

        return $this;
    }

    public function getEnfant(): ?string
    {
        return $this->enfant;
    }

    public function setEnfant(?string $enfant): self
    {
        $this->enfant = $enfant;

        return $this;
    }

    public function getVis(): ?string
    {
        return $this->vis;
    }

    public function setVis(?string $vis): self
    {
        $this->vis = $vis;

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(?string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }

    public function getReligion(): ?string
    {
        return $this->religion;
    }

    public function setReligion(?string $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

    public function getPratique(): ?string
    {
        return $this->pratique;
    }

    public function setPratique(?string $pratique): self
    {
        $this->pratique = $pratique;

        return $this;
    }

    public function getMange(): ?string
    {
        return $this->mange;
    }

    public function setMange(?string $mange): self
    {
        $this->mange = $mange;

        return $this;
    }

    public function getAnimaux(): ?string
    {
        return $this->animaux;
    }

    public function setAnimaux(?string $animaux): self
    {
        $this->animaux = $animaux;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setLifeStyle($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getLifeStyle() === $this) {
                $user->setLifeStyle(null);
            }
        }

        return $this;
    }
}
