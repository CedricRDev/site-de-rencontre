<?php

namespace App\Controller;

use App\Entity\Block;
use App\Entity\User;
use App\Form\SearchFormType;
use App\Repository\BlockRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
       
        $userRepository = $this
        ->getDoctrine()
        ->getRepository(User::class);

        $user = $userRepository
        ->findBy([],[ 'createdAt' => 'DESC'], 3);

        return $this->render('accueil.html.twig',
        [
            "user"=> $user]
        );
    }

    /**
     * @Route("/accueil", name="accueil")
     */
    public function home(UserRepository $userRepository, Request $request, BlockRepository $blockRepository): Response
    {
        // on recupere l'utilisateur connecté
        $userConnect = $this->getUser();
        //dd($userConnect->getId());

        // on recupere le genre de l'utilisateur
        $userGender = $userConnect->getGender();
        $userRepository = $this
        ->getDoctrine()
        ->getRepository(User::class);  

        $blockRepository = $this
        ->getDoctrine()
        ->getRepository(Block::class);   

        
        $blockUserRequest = $blockRepository->findBy(['requestUser'=>$userConnect]);
        $ResultUserBlock = [];
        $resultUser = [];
        $result = [];

            // on va chercher la DB le sexe oppossé au gender de l'utilisateur connecté
            if ($userGender == 'male'){
                    $user = $userRepository
                        ->findBy(['gender' => 'female']);
            }elseif($userGender == 'female'){
                            
                    $user = $userRepository
                        ->findBy(['gender' => 'male']);
            }
            $resultUser = $user;

            foreach( $user as  $users)
            { 
                // on definit l'age de chaque utilisateur a partir de DDN     
                $age = $users->getDdn()->diff(new \DateTime())->format('%y');
                // on range la variable dans set(age) 
                $users->setAge($age);
           
                // on parcourt le tableau des utilisateurs bloques par L'user connecté
              foreach ($blockUserRequest as $blockUserRequests)
                {              
                    // si l on trouve un utiliseur parmis les bloqué de l'user connecte on l'enleve du tableau            
                    if( ($blockUserRequests->getTargetUser()->getId() === $users->getId() ) )
                    { 
                        $ResultUserBlock = $blockUserRequests->getTargetUser();
                        unset($resultUser[array_search($ResultUserBlock, $resultUser) ]);
                    }
                }
            } 
                
            $searchForm =  $this->createForm(SearchFormType::class);
            $searchForm->handleRequest($request);

            if ($searchForm->isSubmitted() && $searchForm->isValid())
            {
                // On recupere les données du formulaire et les attribue a des variables
                $minAge = $searchForm->get('minAge')->getData();
                $maxAge = $searchForm->get('maxAge')->getData();

                // si aucun champ n'est renseigne on affiche tout par défaut
                if ($minAge == null  && $maxAge == null)
                {
                    $user = $userRepository->findAll();
                } else {
                    
                    foreach ($resultUser as $resultUsers)
                    {
                        // on definie leur age
                        $resultAge = $resultUsers->getDdn()->diff(new \DateTime())->format('%y');
                    
                        // si ca match, on les range dans le tableau 
                        if ($resultAge >= $minAge && $resultAge <= $maxAge)
                        {
                            // on push dans un tableau vide le resultat
                            $result [].= $resultUsers;
                        } 
                    } 
                    // on recupere le tableau et on l'assigne a $resultUser
                    $resultUser = $result;
                    
                    //on va chercher dans le Repository l'entité USER
                    $resultUser = $userRepository->findBy(['username' => $resultUser]);    
                }  
            }
            return $this->render('index.html.twig',[
            "user"=> $resultUser,
            "searchForm" => $searchForm->createView()
            ]);
    }       
}