<?php
    namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints\File as ConstraintsFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

    /**
     * @Route("/profile")
     */

class PictureController extends AbstractController {   

    /**
     * @Route("/modify/picture-modify", name="profile_picture_modify")
     */
    public function modifyPicture(Request $request, Filesystem $fs, SluggerInterface $slugger): Response
    {
       // $this->denyAccessUnlessGranted('ROLE_USER');
       $user = $this->getUser();

       $form = $this
       ->createFormBuilder()
       ->add('pictureFile', FileType::class, [
            'label' => 'Profile Picture',
            'mapped' => false,
            'required' => false,
            'constraints'=>[
                new ConstraintsFile([
                    'mimeTypes'=>[
                        'image/*',
                    ],
                    'mimeTypesMessage' => 'Only PNG and JPG are allowed',
                    'maxSize' => '2M'
                ])
            ],
        ])
        ->add('deletePicture', CheckboxType::class, [
            'label'=> 'Delete Picture',
            'required' =>false,
            'mapped'=>false,
            ])
        ->add('submit', SubmitType::class, ['label' => 'Modify Picture'])
        ->getForm();
           
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ){

            $pictureFile = $form->get('pictureFile')->getData();
            $deletePicture = $form->get('deletePicture')->getData();
                if($pictureFile && !$deletePicture)
                {
                // Code issu de la documentation :
                // On slug le fichier et si tout va bien on le range dans le dossier et on l'attribue à l'utilisateur :
                    $originalFilename = pathinfo($pictureFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$pictureFile->guessExtension();

                    try {
                        $pictureFile->move(
                            $this->getParameter('user_profile_pictures_dir'),
                            $newFilename
                        );
                    } catch (FileException $e) {
                        throw new HttpException(500,'erreur upload');
                    }    
                       
                    //Si l'image n'est pas l'image par defaut (ici 'null') : on supprime l'ancienne image
                    if ($user->getPicture() != null )
                    {
                        $newPicture = $this->getParameter('user_profile_pictures_dir') . '/' . $user->getPicture();
                        $fs = new Filesystem();
                    try {
                        $fs->remove($newPicture);
                    } catch (IOException $e) {
                        throw new HttpException(500, 'An error occured during file upload');
                    }
                    }
                       $user->setPicture($newFilename);  
                }
// Si on supprime l'image ET que l'image n'est pas celle par defaut('null') alors on peut supprimer l'image.
// on enregistre ensuite avec la valeur 'null'
            if($deletePicture && !$pictureFile)
            {
                if ($user->getPicture() != null )
                {
                $oldPicture = $this->getParameter('user_profile_pictures_dir') . '/' . $user->getPicture();
                 
                    try{
                        $fs->remove($oldPicture);
                        } catch (IOException $e){
                            throw new HttpException(500, 'an error occured during file delete');
                        }
                        $user->setPicture(null);
                }
            }         
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                
                return $this->redirectToRoute('accueil');
            }
        return $this->render('profile/modifyPicture.html.twig', ['pictureModifyForm' => $form->createView()]);
    }
} 