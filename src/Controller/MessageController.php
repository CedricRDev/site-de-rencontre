<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Conversation;
use App\Repository\ConversationRepository;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/profile/view/{id}")
     */

class MessageController extends AbstractController {

    /**
     * @Route("/conversation/message", name="create_message")
     */
    public function createMessage(int $id, Request $request, UserRepository $userRepository) : Response
    {
        // on créer un nouveau message et une nouvelle conversation       
                $conversation = new Conversation;
                $message = new Message;

                $entityManager = $this
                ->getDoctrine()
                ->getManager();
               
        //Création du nouveau message
                $formCreateMessage = $this
                ->createFormBuilder()
                ->add('content', CKEditorType::class)
                ->add('submit', SubmitType::class, ["label" =>"Send"])
                ->getForm();
        
        // verification du formualire       
                $formCreateMessage->handleRequest($request);
                    if ( $formCreateMessage->isSubmitted() && $formCreateMessage->isValid() )
                    { 
                        //dd($formCreateMessage);
                        $message->addConversation($conversation);

                        //on recupere l'utilisateur connecté
                        $userExpediteur = $this->getUser();
                        $message->setExpediteur($userExpediteur);

                        // on recupere l'id dans l'URL et on le donne au destinataire
                        $userDestinataire= $userRepository->findOneBy(["id"=>$id]);
                        $message->setDestinataire($userDestinataire);

                        // on stocke l'id destinataire dans une session
                        //$this->session->set('destinataireId', 'id');
                       
                        // on attribue la date de creation du message et de la conversation
                        $message->setCreatedAt (new \DateTime());
                        $conversation->setCreatedAt (new \DateTime());

                        /// on attribue un ID(conversation), au message
                        $message->setConversation ($conversation);

                        // je recupere le contenu 'content' du formulaire et le range 
                        $content = $formCreateMessage->get('content')->getData();
                        $message->setContent($content);

                        // si tout va bien on enregistre 
                        if($content){
                            $entityManager->persist($message);
                            $entityManager->persist($conversation);
                            $entityManager->flush();
                            return $this->redirectToRoute('index');
                        }
                    }
                    return $this->render(
                        'echange/messageForm.html.twig',
                        [
                            "messageForm"=> $formCreateMessage->createView()
                        ]
                        );;
    }

     /**
     * @Route("/conversation/new-message", name="new_message")
     */
    public function newMessage(int $id, Request $request, ConversationRepository $conversationRepository, MessageRepository $messageRepository) : Response
    {

        // on recherche tous les messages
        $messages = $messageRepository->findall();

        // on attribue via l'URL l'ID de la conversation:
        $conv= $conversationRepository->findOneBy(["id"=>$id]);
        
       
            foreach ($messages as $message)
            {
                // Si la conversation existe deja :
                if ($conv->getId() == $message->getConversation()->getId())
                {
                    $newMessage = new Message;
                    $entityManager = $this
                    ->getDoctrine()
                    ->getManager();
                                        
                    $formResponseMessage = $this
                    ->createFormBuilder()
                    ->add('content', CKEditorType::class)
                    ->add('submit', SubmitType::class, ["label" =>"Send your response"])
                    ->getForm();
                    $formResponseMessage->handleRequest($request);
                        
                        if ( $formResponseMessage->isSubmitted() && $formResponseMessage->isValid() )
                        {  
                            //on recupere l'utilisateur connecté et on attribue a l'expediteur
                            $userExpediteur = $this->getUser();
                            $newMessage->setExpediteur($userExpediteur);

                            // on recupere l'id destinataire existant et on l'attribue au new message
                            $userDestinataire= $message->getExpediteur();
                         //   dd($userDestinataire);
                            $newMessage->setDestinataire($userDestinataire);

                            // on attribue l'iD de la conversation existante trouve avec le repository
                            $newMessage->setConversation ($conv);
                          
                            // on attribue la date de creation du message
                            $newMessage->setCreatedAt (new \DateTime());

                            // on recupere le contenu 'content' du formulaire et le range 
                            $content = $formResponseMessage->get('content')->getData();
                            $newMessage->setContent($content);

                                // si tout va bien on persist et flush
                                if($content){
                                    $entityManager->persist($newMessage);
                                    
                                    $entityManager->flush();
                                    return $this->redirectToRoute('conversation_by_id', ["id"=>$id]);
                                }
                        }
                            return $this->render(
                            'echange/messageForm.html.twig',
                            [
                                "messageForm"=> $formResponseMessage->createView()
                            ]
                            );
                    }         
            }
            return $this->redirectToRoute('accueil') ; 
    }        
}