<?php
    namespace App\Controller;

use App\Form\PasswordModifyFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/profile")
     */

class PasswordController extends AbstractController
{
     /**
     * @Route("/modify/password-modify", name="profile_password_modify")
     */
    public function modifyPassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        // $this->denyAccessUnlessGranted("ROLE_USER");
        $user = $this->getUser();
        $form = $this->createForm(PasswordModifyFormType::class, $user);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $encoder -> encodePassword($user, $form->get('newPassword')
                                                        ->getData()
                                          ) 
            );
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('accueil');
            }
            return $this->render(
                'profile/modifyPassword.html.twig',
             ["passwordModifyForm" => $form->createView()]);
    } 
}     