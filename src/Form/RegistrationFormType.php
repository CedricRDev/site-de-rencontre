<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File as ConstraintsFile;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('gender', ChoiceType::class, [ 
                'choices' => [
                    'male' => 'male',
                    'female'=> 'female',
                ],
                'choice_attr' => [
                    'male' => ['data-color' => 'blue'],
                    'female' => ['data-color' => 'pink'],
                ],  
            ])
            ->add('email')
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('description', null,[
                'required' => false,
                'empty_data'=> 'Cet utilisateur doit être timide.'
            ]) 
            ->add('ville', null,[
                'required' => true,
                'empty_data'=> 'Cet utilisateur doit être timide.',
                'constraints' => [
                    new NotBlank([
                        'message'=>'Please enter a city'
                    ])
                ]
            ])
            ->add('ddn', BirthdayType::class,[
                'mapped' => true,
                'required' => true,
            ])
            ->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'first_options' => ['label'=>'Password'],
                'second_options' => ['label'=>'Repeat Password'],
            ])
            ->add('pictureFile', FileType::class, [
                'label' => 'Profile Picture',
                'mapped' => false,
                'required' => false,
                'constraints'=>[
                    new ConstraintsFile([
                        'mimeTypes'=>[
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Only PNG and JPG are allowed',
                        'maxSize' => '2M'
                    ])
                    ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
