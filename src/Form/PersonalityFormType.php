<?php

namespace App\Form;

use App\Entity\Personality;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonalityFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        // Section de la personalite
            ->add('relation', ChoiceType::class, [
                'choices'=> [
                    'Une relation serieuse' => 'Une relation serieuse',
                    'Une relation sans lendemain'=> 'Une relation sans lendemain',
                    'On verra'=> 'On verra'
            ]])
             ->add('romantique', ChoiceType::class, [
                'choices'=> [
                    'Tres Romantique' => 'Tres Romantique',
                    'Moyennement'=> 'Moyennement',
                    'Mais pas du tout'=> 'Mais pas du tout'
             ]])
             ->add('mariage', ChoiceType::class, [
                'choices'=> [
                    'Je veux me marier' => 'Je veux me marier',
                    'Tout dépendra '=> 'Tout dépendra ',
                    'Ce n\'est plus ou pas pour moi'=> 'Ce n\'est plus ou pas pour moi'
             ]])
             ->add('enfant', ChoiceType::class, [
                'choices'=> [
                    'j\'en ai et n\'en veut plus' => 'j\'en ai et n\'en veut plus',
                    'J\'en veut '=> 'J\'en veut ',
                    'les couches c\'est  pas pour moi'=> 'les couches c\'est  pas pour moi'
             ]])
             ->add('submit', SubmitType::class, [
                'label' => 'Modify Personality'
            ])
            ->getForm();
             ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> Personality::class
        ]);
    }
}