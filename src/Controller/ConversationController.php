<?php

namespace App\Controller;

use App\Entity\Block;
use App\Entity\Message;
use App\Entity\Conversation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/profile")
     */


class ConversationController extends AbstractController
{
    /**
     * @Route("/conversation", name="conversation")
     */
    
  public function viewConversation() :Response
   {   
        $user = $this->getUser()->getId();

        $conversationRepository = $this
        ->getDoctrine()
        ->getRepository(Conversation::class);
        $conversation = $conversationRepository->findAll();

        $resultConversation=[];
        $resultBlock =[];

        $blockRepository = $this
        ->getDoctrine()
        ->getRepository(Block::class);
        $block = $blockRepository->findBy(['requestUser' =>$user]);
     
        foreach( $conversation as $conversations){

                // Si la conversation existe :
               if($conversations->getMessage() != null)
                {
                    //on range les conversation dans un tableau
                    $resultConversation[] = $conversations;

                    foreach($block as $blocks)
                    {
                        // si L'id du destinataire de la conversation match avec l'id des users bloqués
                        if( $conversations->getMessage()->getDestinataire()->getId() === $blocks->getTargetUser()->getid()  ){
                       
                         //on range cette conversation dans un tableau   
                        $resultBlock = $conversations;
                    
                        // on enleve la conversation du tableau de resultat final
                        unset($resultConversation[array_search($resultBlock, $resultConversation) ]);
                        }
                    }
                }   
            }

        return $this->render('echange/conversation.html.twig',
        [
            "conversation"=> $resultConversation]
        );
    }

    /**
     * @Route("/conversation/view/{id}", name="conversation_by_id")
     */
    
    public function conversationById(int $id) :Response
    {
        $conversationRepository = $this
        ->getDoctrine()
        ->getRepository(Conversation::class);      
        $conversation = $conversationRepository-> find($id);

        $messageRepository = $this
        ->getDoctrine()
        ->getRepository(Message::class);      
        $message = $messageRepository-> findAll();
      
         return $this->render('echange/message.html.twig',
         [
            "conversation"=> $conversation,
            "message"=> $message,
         ]);
    }

    /** 
     * @Route("/conversation/delete/{id}", name="conversation_delete")
     */
    public function deleteConversation(int $id):Response
    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $conversation = $entityManager->getRepository(Conversation::class)->find($id);
       
        $messageRepository = $this
        ->getDoctrine()
        ->getRepository(Message::class);      
        $message = $messageRepository-> findBy(['conversation' =>$id]);
        $messages = $message["0"];
      
            if( $conversation->getId() === $messages->getConversation()->getId()) {;

                $messages->removeConversation($conversation);
            }
       
        $entityManager->flush();

        return $this->redirectToRoute('conversation');
    }
  
}