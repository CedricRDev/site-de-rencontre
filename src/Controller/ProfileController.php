<?php
namespace App\Controller;

use App\Entity\Notation;
use App\Entity\User;
use App\Repository\NotationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

    /**
     * @Route("/profile")
     */

class ProfileController extends AbstractController
{
    /**
     * @Route("/view/{id}",name="profile_view")
     */
    public function view(int $id, UserRepository $userRepository, NotationRepository $notationRepository) : Response
    {
        $user= $userRepository->findOneBy(["id"=>$id]);
            if(!$user){
                $this->createNotFoundException('no user found');
            }
     $notationRepository = $this
                ->getDoctrine()
                ->getRepository(Notation::class);
              
    $notationAll = $notationRepository->findBy(["rateUser"=>$id]);
    $i = 0;
    $notationMoy =[];
    foreach ($notationAll as $notationAlls){
        $i++;
        $notationMoy [] = $notationAlls->getRate() ;
    }
    
    $moyenne = array_sum($notationMoy)/$i;

        return  $this->render('profile/view.html.twig',
        [
            "notation" => $notationAll,
            "average" => $moyenne,
            "user"=> $user
        ]);
    }

    /** 
     * @Route("/modify/{id}", name="profile_modify")
     */
    public function updateProfile( int $id, Request $request):Response 
    {
       
        $entityManager = $this
            ->getDoctrine()
            ->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);
      
        $formUpdate= $this->createFormBuilder($user)
            ->add('description', CKEditorType::class)
            ->add('ville')
            ->add('submit', SubmitType::class, ["label" =>"Update Profile"])
            ->getForm();
          
        $formUpdate->handleRequest($request);
           
            if ($formUpdate->isSubmitted() && $formUpdate->isValid())
            {
                $description = $formUpdate->get('description')->getData();
                $ville = $formUpdate->get('ville')->getData();                
                $user->setDescription($description);
                $user->setVille($ville);

               if($description || $ville)
                $entityManager->persist($user);
                $entityManager->flush();
                return $this->redirectToRoute('accueil', 
                [
                    'id' => $user->getId()
                ]);
            }
        return $this->render(
            'profile/update.html.twig',
            [
                "updateForm"=> $formUpdate->createView()
            ]
            );
    }
    /** 
     * @Route("/delete/{id}", name="profile_delete")
     */
    public function deleteProfile(int $id):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No product found for id '. $id
            );
        }

        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('accueil');
    }
   
   
}