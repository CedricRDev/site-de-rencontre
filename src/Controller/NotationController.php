<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Notation;
use App\Form\NotationFormType;
use App\Repository\MessageRepository;
use App\Repository\NotationRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/profile/view/{id}")
     */
class NotationController extends AbstractController{
    
    /**
     * @Route("/rate", name="create_rate")
     */
    public function createRate(Request $request, int $id, UserRepository $userRepository, NotationRepository $notationRepository, MessageRepository $messageRepository)
    {
        $userConnect = $this->getUser();
        $userRate = $userRepository->findOneBy(["id"=>$id]);
             
        $notationRepository = $this
        ->getDoctrine()
        ->getRepository(Notation::class);

        $messageRepository = $this
        ->getDoctrine()
        ->getRepository(Message::class);

        $entityManager = $this
                    ->getDoctrine()
                    ->getManager();

        //Création du nouvelle note
        $note = new Notation;
        $formCreateNote = $this->createForm(NotationFormType::class, $note);
        $formCreateNote->handleRequest($request);

        $message = $messageRepository->findBy(['destinataire' =>$userConnect]);

        // on recupere les notations réalisé par l'utilisateur connecté->OK
        $notation = $notationRepository->findBy(['user' =>$userConnect->getId()]);
      
        //mise en place d'une condition de vote unique
        foreach ($notation as $notations) {
            if ( ($notations->getUser()->getId() === $userConnect->getId()) && $notations->getRateUser()->getId() === $userRate->getId() )
            {
                throw $this->createNotFoundException(
                    "Vous ne pouvez notez qu'une fois cet utilisateur"
                );
            }
        }
        // On parcourt le tableau et on enregiste chaque ID
        foreach($message as $messages)
        { 
            $userSearchId = $messages->getExpediteur()->getId();
            $userRateId = $userRate->getId();
        }
        // Si l'utilisateur NOTé a envoyé un message a l'utilisateur qui note
        if($userSearchId === $userRateId )
        {
                if ( $formCreateNote->isSubmitted() && $formCreateNote->isValid() )
                { 
                    // on recupere le contenu du formulaire 
                    $content = $formCreateNote->get('content')->getData();
                    $rate = $formCreateNote->get('rate')->getData();
        
                    // on l'attribue l'utilisateur connecté a celui qui note
                    $note->setUser($userConnect);
        
                    // on recupere l'id dans l'URL et on le donne au destinataire
                    $note->setRateUser($userRate);
        
                    //on recupere le contenu de commentaire
                    $note->setContent($content);
        
                    // on recupere la note
                    $note ->setRate($rate); 
        
                    if($rate >= "0" && $rate <= "10"){
        
                        $entityManager->persist($note);
                        $entityManager->flush();
                        return $this->redirectToRoute('accueil');
                    }
                    // Si pas d'echange, on renvoit une erreur
                    else {
                        throw $this->createNotFoundException(
                            'la note doit etre comprise entre 1 et 10 '
                        );
                    }
                }
        }elseif($userSearchId != $userRateId )
        {
        throw $this->createNotFoundException(
                "Merci d'echanger avec cet utilisateur"
        );
        }     

        return $this->render(
            'profile/notationForm.html.twig',
            [
                "noteForm"=> $formCreateNote->createView(),
                "user"=>$userConnect,
                "userRate"=>$userRate,
            ]
            );             
    }
    
     
}