<?php

namespace App\Form;

use App\Entity\Notation;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder    
        ->add('rate', NumberType::class,[
            'required'=> true,
            'mapped'=>true,
            'attr'=> ['min'=>0,"max"=>10]
        ])
        ->add('content', CKEditorType::class,[
            'required'=> false,
            'mapped'=>true,
            'empty_data'=>'',
        ])
        ->add('rating', SubmitType::class, ["label" =>"Send"])
        ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Notation::class,
        ]);
    }
}