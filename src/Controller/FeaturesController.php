<?php

namespace App\Controller;

use App\Entity\LifeStyle;
use App\Entity\Personality;
use App\Entity\Physique;
use App\Form\LifeStyleFormType;
use App\Form\PersonalityFormType;
use App\Form\PhysiqueFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route("/profile/modify/{id}")
     */
    
class FeaturesController extends AbstractController
{
    /**
     * @Route("/features/personality",name="profile_features_personality")
     */
    public function modifyPersonality(Request $request): Response
    {
        // On récupere les données de l'utilisateur et on crée 3 nouvelles class, (Personality, Physique, LifeStyle)
        $user = $this->getUser();
        $personality = new Personality;
        $physique = new Physique;
        $lifeStyle = new LifeStyle;
        $entityManager = $this
            ->getDoctrine()
            ->getManager();

       // Creation du formulaire sur la Personality :
        $formPersonality = $this->createForm(PersonalityFormType::class, $personality);
        
        $formPersonality->handleRequest($request);
            if ($formPersonality->isSubmitted() && $formPersonality->isValid())
            {
                $personality->addUser( $user);
                // on récupere les valeurs des champs et on les range dans les variables    
                $relation = $formPersonality->get('relation')->getData();
                $romantique = $formPersonality->get('romantique')->getData();
                $mariage = $formPersonality->get('mariage')->getData();
                $enfant = $formPersonality->get('enfant')->getData();

                if($relation && $romantique && $mariage && $enfant)
                {
                    $entityManager->persist($personality);
                    $entityManager->flush();
                    return $this->redirectToRoute('accueil');
                }
            }
               
         // Creation du formulaire sur le physique :
                $formPhysique = $this->createForm(PhysiqueFormType::class, $physique);
                
                $formPhysique->handleRequest($request);
                if ($formPhysique->isSubmitted() && $formPhysique->isValid())
                {
                    $physique->addUser( $user);
                    // on récupere les valeurs des champs et on les range dans les variables
                    $taille = $formPhysique->get('taille')->getData();
                    $silhouette = $formPhysique->get('silhouette')->getData();
                    $style = $formPhysique->get('style')->getData();
                    $origine = $formPhysique->get('origine')->getData();
                    $yeux = $formPhysique->get('yeux')->getData();
                    $cheveux = $formPhysique->get('cheveux')->getData();
                    $longueur_cheveux = $formPhysique->get('longueur_cheveux')->getData();
                // $poids = $formPhysique->get('poids')->getData();

                    if($taille &&  $silhouette && $style && $origine && $yeux && $cheveux && $longueur_cheveux )
                    {
                        $entityManager->persist($physique);
                        $entityManager->flush();
                        return $this->redirectToRoute('accueil');
                    }
                }

            // Creation du formulaire sur le Mode de vie :
                $formLifeStyle = $this->createForm(LifeStyleFormType::class, $lifeStyle);
                
                $formLifeStyle->handleRequest($request);
                if ($formLifeStyle->isSubmitted() && $formLifeStyle->isValid())
                {
                    $lifeStyle->addUser($user);
                    // on récupere les valeurs des champs et on les range dans les variables
                    $statut = $formLifeStyle->get('statut')->getData();
                    $fume = $formLifeStyle->get('fume')->getData();
                    $enfant= $formLifeStyle->get('enfant')->getData();
                    $vis= $formLifeStyle->get('vis')->getData();
                    $profession= $formLifeStyle->get('profession')->getData();
                    $religion= $formLifeStyle->get('religion')->getData();
                    $pratique= $formLifeStyle->get('pratique')->getData();
                    $mange= $formLifeStyle->get('mange')->getData();
                    $animaux= $formLifeStyle->get('animaux')->getData();
                
                    if($statut && $fume && $enfant &&  $vis && $profession && $religion && $pratique && $mange && $animaux)
                    {
                        $entityManager->persist($lifeStyle);
                        $entityManager->flush();
                        return $this->redirectToRoute('accueil');
                    }
                }

        // On envoie les 3 formulaires sur le template "modifyFeature.html.twig"
                return $this->render(
                    'profile/modifyFeatures.html.twig',
                    [
                        "modifyPhysiqueForm" => $formPhysique->createView(),
                        "modifyPersonalityForm" => $formPersonality->createView(),
                        "modifyLifeStyleForm" => $formLifeStyle->createView(),
                    ]
                );       
        return $this->redirectToRoute('accueil');
    }
}     