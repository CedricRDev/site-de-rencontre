<?php

namespace App\Form;

use App\Entity\LifeStyle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LifeStyleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        // Section life style
        ->add('statut',ChoiceType::class, [
            'choices'=> [
                'Célibataire' => 'Célibataire',
                'Divorcé'=> 'Divorcé',
                'Veuf'=> 'Veuf'
        ]])
        ->add('fume',ChoiceType::class, [
            'choices'=> [
                'Beaucoup' => 'Beaucoup',
                'Un peu'=> 'Un peu',
                'Essaye d\'arreter'=> 'Essaye d\'arreter',
                'Non'=> 'Non',
        ]])
        ->add('enfant',ChoiceType::class, [
            'choices'=> [
                'j\'en ai et n\'en veut plus' => 'j\'en ai et n\'en veut plus',
                'J\'en veut '=> 'J\'en veut ',
                'les couches c\'est  pas pour moi'=> 'les couches c\'est  pas pour moi'
        ]])
        ->add('vis',ChoiceType::class, [
            'choices'=> [
                'Seul(e)' => 'Seul(e)',
                'En colocation'=> 'En colocation',
                'Chez mes parents'=> 'Chez mes parents',
                'Avec mes enfants'=> 'Avec mes enfants'     
        ]])        
        ->add('profession',TextType::class)           
        ->add('religion',ChoiceType::class, [
            'choices'=> [
                'Catholique' => 'Catholique',
                'Musulman'=> 'Musulman',
                'Juif'=> 'Juif',
                'Agnostique'=>'Agnostique',
                'Aucune'=>'Aucune',
        ]])       
        ->add('pratique',ChoiceType::class, [
            'choices'=> [
                'Tous les jours' => 'Tous les jours',
                'Un peu'=> 'Un peu',
                'Pas du tout'=> 'Pas du tout',
        ]])       
        ->add('mange',ChoiceType::class, [
            'choices'=> [
                'De tout' => 'De tout',
                'Végetarien'=> 'Végetarien',
                'Veuf'=> 'Veuf',
                'Diet'=>'Diet',
                'Casher'=>'Casher',
                'halall'=>'halall',
        ]])
        ->add('animaux',ChoiceType::class, [
            'choices'=> [
                'Chien' => 'Chien',
                'Chat'=> 'Chat',
                'Autres'=> 'Autres',
        ]])        
        ->add('submit', SubmitType::class, [
        'label' => 'Modify Life Style'
        ])
        ->getForm();
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'=> LifeStyle::class
        ]);
    }
}