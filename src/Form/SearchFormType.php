<?php

namespace App\Form;

use App\Entity\Search;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minAge', IntegerType::class,[
                'label' => 'Age minimum :',
               // 'placeholder'=> 'age min',
                'mapped'=> 'false',
                'required'=> 'false',
            ])
            ->add('maxAge', IntegerType::class,[
                'label' => 'Age maximum :',
                'mapped'=> 'false',
                //'placeholder'=> 'age max',
                'required'=> 'false',
            ])
            ->add('submit', SubmitType::class,[
                'label'=> 'Search',
                ])
            ->getForm(); 
             
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Search::class,
        ]);
    }
}